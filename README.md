# CoffeeWeb

## Description
This project is a demo project to show the functionality of an STS pressure sensor in combination with a Raspberry Pi and LabVIEW. This project uses libraries from Sam Sharp for the RaspberryPi, especially the "LINX Raspberry Pi Addons by MediaMongrels - Toolkit for LabVIEW Download" and the "WebSockets" Libraries.

## Installation
You will need the NI LabVIEW version 21 or higher as well as the NI G Web Development Software.
The dependencies are in the .vipc installabe with the help of the VI Package Manager.


## Usage
If you want to use this project with your RaspberryPI, then you need to deploy the files with LabVIEW. Please follow the following steps:
1. Open the Project in LabVIEW
2. Replace the RaspberryPI in the project with your own
3. Run the Build on your RaspberryPi as Startup
4. You can now access the WebService under: http://localhost:8002/CoffeeWeb/CoffeeWeb.html


## Helper Libraries
- ["LINX Raspberry Pi Addons by MediaMongrels - Toolkit for LabVIEW Download"](https://www.vipm.io/package/mediamongrels_lib_linx_raspberry_pi_addons/) - MediaMongrels Ltd
- ["Caraya Unit Test Framework"](https://www.ni.com/de-ch/support/downloads/tools-network/download.caraya-unit-test-framework.html#378006) - JKI
- ["WebSockets"](https://www.vipm.io/package/mediamongrels_ltd_lib_websockets_api/) - MediaMongrels Ltd

## Contributing
We are open for your Inputs. For an easy process, please follow these 3 simple Steps:

1. If you want to add feature or change behavoir, create an issue so we can discuss about it
2. Fork the repo
3. Create a merge request with your changes


## Authors and acknowledgment
Philipp Lichtleitner - STS Sensor Technik Sirnach AG


## License
Everyone is permitted to run, study, share, and modify the software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## Project status
In developement.
